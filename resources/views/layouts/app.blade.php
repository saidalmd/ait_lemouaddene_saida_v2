<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <title>Document</title>
</head>
<body>
    <header>
        <nav>
            <a href="{{route('voitures.index')}}">Gestion voitures</a>
            <a href="#">Gestion marques</a>
        </nav>
    </header>
    <div id="container">
        @yield('content')
    </div>
    <footer>
        name website
    </footer>
</body>
</html>