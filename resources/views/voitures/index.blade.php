@extends('layouts.app')
@section('content')
<a href="{{route('voitures.create')}}" id="ajouter">Ajouter une voiture</a>
<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Immatriculation</th>
            <th>Nombre chevaux</th>
            <th>Description</th>
            <th>Marque</th>
            <th>Image</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($voitures as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->immatriculation}}</td>
                <td>{{$item->nbrChevaux}}</td>
                <td>{{$item->description}}</td>
                <td>{{$item->marque->nom}}</td>
                <td> <img src="{{asset('storage/'.$item->image)}}" alt="#"> </td>
                <td>
                    <a href="{{route('voitures.show',$item->id)}}">Detail</a>
                    <a href="{{route('voitures.edit',$item->id)}}">Editer</a>
                    <form action="{{route('voitures.destroy',$item->id)}}" method="post">
                        @method('delete')
                        @csrf
                        <input type="submit" value="Supprimer">
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
    
@endsection