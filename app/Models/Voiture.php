<?php

namespace App\Models;

use App\Models\Marque;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Voiture extends Model
{
    use HasFactory;
    protected $fillable=['immatriculation','nbrChevaux','description','marque_id'];

    public function marque(){
        return $this->belongsTo(Marque::class);
    }
}
