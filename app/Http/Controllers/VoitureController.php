<?php

namespace App\Http\Controllers;

use App\Models\Marque;
use App\Models\Voiture;
use Illuminate\Http\Request;

class VoitureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $voitures=Voiture::paginate(20);
        return view("voitures.index",compact('voitures'));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $marques=Marque::select('id','nom')->get();
        return view('voitures.create',compact('marques'));

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $validated_data=$request->validate([
            "immatriculation"=>'required',
            "nbrChevaux"=>'required',
            "description"=>"required",
            "marque_id"=>"required",
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg,webp|max:80000',
        ]);
        if($request->hasFile('image')){
            $imagePath=$request->file('image')->store('products/images','public');
            $validated_data['image']=$imagePath;
        }
        Voiture::create($validated_data);
        return redirect()->route('voitures.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
        $voiture=Voiture::find($id);
        return view("voitures.show",compact('voiture'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        $voiture=Voiture::find($id);
        $marques=Marque::select('id','nom')->get();
        return view('voitures.edit',compact('voiture','marques'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $validated_data=$request->validate([
            "immatriculation"=>'required',
            "nbrChevaux"=>'required',
            "description"=>"required",
            "marque_id"=>"required"
        ]);
        $voiture=Voiture::find($id);
        $voiture->update($validated_data);
        return redirect()->route('voitures.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $voiture=Voiture::find($id);
        $voiture->delete();
        return redirect()->route('voitures.index');
    }
}
